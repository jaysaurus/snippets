using System;
using UnityEngine;

public class CheckpointController : MonoBehaviour
{
  public Sprite redFlag;
  public Sprite greenFlag;
  private SpriteRenderer checkpointSpriteRenderer;
  public bool checkpointReached = false;

  // Start is called before the first frame update
  void Start()
  {
    checkpointSpriteRenderer = GetComponent<SpriteRenderer>();
  }

  // Update is called once per frame
  void Update()
  {

  }

  private void toggleActiveCheckpoint(GameObject obj)
  {
    Collider2D col = obj.GetComponent<Collider2D>();
    if (col.name != this.name)
    {
      CheckpointController c = obj.GetComponent<CheckpointController>();
      if (c.checkpointReached)
      {
        SpriteRenderer sr = obj.GetComponent<SpriteRenderer>();
        sr.sprite = redFlag;
        c.checkpointReached = false;
      }
    }
    else
    {
      checkpointSpriteRenderer.sprite = greenFlag;
      checkpointReached = true;
    }
  }

  private void OnTriggerEnter2D(Collider2D other)
  {
    if (other.tag == "Player" && !checkpointReached)
    {
      GameObject[] flags = GameObject.FindGameObjectsWithTag("Checkpoint");
      Array.ForEach(flags, toggleActiveCheckpoint);
    }
  }
}

